import * as vscode from 'vscode';

export class GitlabCodeLens extends vscode.CodeLens {
	key: string;

	constructor(range: vscode.Range, key: string, command?: vscode.Command) {
		super(range, command);
		this.key = key;
	}
}
